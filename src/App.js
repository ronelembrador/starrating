import './App.css';
import StarRating from './components/StarRating'

function App() {
  return (
    <div>
      <StarRating />
    </div>
  );
}

export default App;
